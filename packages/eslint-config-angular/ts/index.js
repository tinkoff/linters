module.exports = {
  extends: [
    './base',
    './base-typescript',
    './import',
    './member-ordering',
    './line-statements',
    './extraneous-class',
  ],
};
